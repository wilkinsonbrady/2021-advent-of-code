use std::env;
use std::fs;
use std::process;

//fn check_board()

#[derive(Copy, Clone)]
struct Board {
    score: i32,
    turns: i32,
    id: i32,
}

fn main() {
    //Get File Name
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("File name not given");
        process::exit(1);
    }
    let filename = &args[1];
    println!("Input File: {}", filename);

    //Read in data
    let data = fs::read_to_string(filename).expect("File failed to read");

    //Part 1
    // Oh boy, actually using enumerate properly today, unlike the day before and we actually semi
    // know how to use iterators now but not really... still learning, but still
    // Get data inputs - val is kinda eh, but we're learning ¯\_(ツ)_/¯
    let mut val = data.lines();
    let inputs = val.nth(0).unwrap();
    println!("Inputs: {}", inputs);

    //Deal with bingo boards
    let mut boards: Vec<Board> = Vec::new();
    let mut curr_board: Vec<Vec<i32>> = Vec::new();
    
    let mut board_num = 0;
    let data_len = data.lines().count();
    for (i,s) in val.skip(1).enumerate() {

        //End of board, check for win count
        if s.len() == 0 || i == data_len-1 {
            
            //Run data through board checker
            //check_board()

            curr_board = Vec::new();
            board_num += 1;

        //end not found yet, break up into rows and columns
        } else {
            print!("{}: {} ", i, s);
            curr_board.push(s.split_whitespace() 
                .map(|n| n.parse().unwrap())
                .collect());
        }

        println!("");
    }

    //Setup vars for next part
    let mut winning_board = Board {
        score: 0,
        turns: 0,
        id: 0,
    };
    let mut first = true;

    //Check for winning board
    for x in boards {
        if first {
            winning_board = x;
            first = false;
        }

        if winning_board.turns > x.turns {
            winning_board = x;
        }
    }

    println!("Part1: Winning board is Board #{} | Turns: {} | Score: {}", winning_board.id, winning_board.turns, winning_board.score);


}




































