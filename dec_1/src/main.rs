use std::env;
use std::fs;
use std::process;

fn main() {
    //Get File Name
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("File name not given");
        process::exit(1);
    }
    let filename = &args[1];
    println!("Input File: {}", filename);

    //Read in data
    let data = fs::read_to_string(filename).expect("File failed to read");

    let data: Vec<i32> = data
        .split_whitespace()
        .map(|s| s.parse().expect("parse error"))
        .collect();

    //Process data for part 1
    let mut first_val = true;
    let mut prev = 0;
    let mut larger = 0;
    for val in &data {
        //This seems horrible, but don't feel reading on a better way ¯\_(ツ)_/¯
        let value = *val;

        //Compare current value to prev
        print!("{}", value);
        if !first_val {
            if prev < value {
                larger += 1;
                println!(" (Increased, total num: {})", larger);
            } else {
                println!(" ---");
            }
        } else {
            println!(" (N/A no prev)");
            first_val = false;
        }

        //Update prev value
        prev = value;
    }

    println!("==================================");

    //Process data for part 2
    let window_size = 3;
    let mut first_val = true;
    let mut i = 0;
    let mut prev = 0;
    let mut count = 0;
    while i < data.len() - 2 {
        // Add up values over window
        let total = data[i..i + window_size].iter().fold(0, |acc, x| acc + x);
        print!("{}", total);

        //Compare current to prev values
        if total > prev && !first_val {
            count += 1;
            println!(" (Increase)");
        } else {
            println!(" ---");
        }

        //Update interator and prev val
        prev = total;
        first_val = false;
        i += 1;
    }

    //Output Answers
    println!("Total Number of larger values (Part 1): {}", larger);
    println!("Total Number of larger values (Part 2): {}", count);
}
