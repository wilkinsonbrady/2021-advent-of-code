use std::env;
use std::fs;
use std::process;


fn main() {
    //Get File Name
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("File name not given");
        process::exit(1);
    }
    let filename = &args[1];
    println!("Input File: {}", filename);

    //Read in data
    let data = fs::read_to_string(filename).expect("File failed to read");

    //Part 1
    let mut bit_count0 = Vec::new();
    let mut bit_count1 = Vec::new();
    let mut step_num = 0;
    for s in data.split("\n") {
        print!("{}: {} ",step_num, s);
        if s.len() > 2 {
            for (i, val) in s.chars().enumerate() {

                // Not great, but eh it works
                if i == bit_count0.len() {
                    bit_count0.push(0);
                    bit_count1.push(0);
                }

                match val {
                    '0' => {
                        bit_count0[i] += 1;
                        print!("z");
                    },
                    '1' => {
                        bit_count1[i] += 1;
                        print!("o");
                    },
                    _ => panic!(),
                }
            }
            println!("");
        } else {
            println!("Oh God, oh no, something might be wrong... or it's the end of the file and I don't feel like messing with it");
        }
        step_num += 1;
    }

    print!("bit_count0 vals: ");
    for x in &bit_count0 {
        print!(" {},", x);
    }
    println!("");

    print!("bit_count1 vals: ");
    for x in &bit_count1 {
        print!(" {},", x);
    }
    println!("");

    //Def depends on arrays being some length and is janky, but eh here we go
    let mut gamma: String = "".to_owned();
    let mut epsilon: String = "".to_owned();
    let mut i = 0;
    while i < bit_count0.len() {
        if bit_count0[i] > bit_count1[i] {
            gamma.push_str("0");
            epsilon.push_str("1");
        } else {
            gamma.push_str("1");
            epsilon.push_str("0");
        }

        i += 1;
    }

    let gamma_int = isize::from_str_radix(&gamma, 2).unwrap();
    let epsilon_int = isize::from_str_radix(&epsilon, 2).unwrap();

    println!("--------------");
    println!("Part1: ");
    println!("Gamma: {} ({}) | Epsilon: {} ({}) | PowerConsumption: {}", gamma_int, gamma, epsilon_int, epsilon, gamma_int * epsilon_int);
    println!("--------------");
}




















