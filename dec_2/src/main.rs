use std::env;
use std::fs;
use std::process;


fn main() {
    //Get File Name
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("File name not given");
        process::exit(1);
    }
    let filename = &args[1];
    println!("Input File: {}", filename);

    //Read in data
    let data = fs::read_to_string(filename).expect("File failed to read");

    //Part 1
    let mut x = 0;
    let mut y = 0;
    let mut step_num = 0;
    for s in data.split("\n") {
        print!("{}: {}",step_num, s);
        if s.len() > 2 {
            let command: Vec<&str> = s.split_ascii_whitespace().collect();
            let direction: &str = command[0];
            let distance: i32 = command[1].parse().unwrap();

            match direction.chars().nth(0).unwrap() {
                'f' => x += distance,
                'd' => y += distance,
                'u' => y -= distance,
                _ => panic!(),
            }
            println!(" X: {} | Y: {} | Moving", x, y);
        } else {
            println!("Oh God, oh no, something might be wrong");
        }
        step_num += 1;
    }

    //Part 2
    let mut hor_dis = 0;
    let mut aim = 0;
    let mut depth = 0;
    let mut step_num = 0;
    for s in data.split("\n") {
        if s.len() > 2 {
            let command: Vec<&str> = s.split_ascii_whitespace().collect();
            let direction: &str = command[0];
            let distance: i32 = command[1].parse().unwrap();

            match direction.chars().nth(0).unwrap() {
                'f' => {
                    hor_dis += distance;
                    depth += aim * distance;
                },
                'd' => aim += distance,
                'u' => aim -= distance,
                _ => panic!(),
            }
        } else {
            println!("Oh God, oh no, something might be wrong");
        }
        println!("{}: {} aim {} | hor {} | depth {}",step_num, s, aim, hor_dis, depth);
        step_num += 1;
    }


    println!("distance: {} | depth: {} | part1 answer: {}", x, y, x*y);
    println!("distance: {} | depth: {} | aim: {} | part2 answer: {}", hor_dis, depth, aim, depth*hor_dis);





}
